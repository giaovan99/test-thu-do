class Data {
  // Khai báo constructor
  constructor() {}

  // Phương thức fetchJSON của DataFetcher
  async fetchJSON() {
    try {
      // Sử dụng fetch để tải dữ liệu từ tệp JSON
      const response = await fetch("../../assets/data/data.json");

      // Kiểm tra xem fetch có thành công không
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      // Trả về dữ liệu JSON
      return response.json();
    } catch (error) {
      // Xử lý lỗi nếu fetch không thành công
      console.error("Error fetching data:", error);
      throw error; // Chuyển tiếp lỗi cho caller nếu muốn xử lý ở nơi gọi
    }
  }
}
